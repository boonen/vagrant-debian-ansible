#!/bin/bash
# use -f to force (re)creation of a box

if [ "$#" -gt 0 ] && [ "$@" == "-f" ]
  then
    vagrant destroy -f
    rm -rf .vagrant *.box
fi

vagrant up

vagrant ssh -c "sudo apt-get update && sudo apt-get install -y --no-install-recommends unzip curl jq apt-transport-https ca-certificates git unzip python-dev libxml2-dev libxslt-dev libffi-dev libssl-dev"

vagrant ssh -c "sudo apt-key update && sudo apt-get update && apt-get dist-upgrade -y && sudo apt-get upgrade -y && sudo apt-get autoremove -y"
vagrant ssh -c "wget -O /tmp/get-pip.py https://bootstrap.pypa.io/get-pip.py && sudo python /tmp/get-pip.py && pip install ansible dnspython"
vagrant ssh -c "sudo apt-get purge perl aptitude-doc-en doc-debian docutils-doc -y && sudo find /var/log -type f -delete"
vagrant ssh -c "sudo aptitude purge ~c -y && sudo apt-get clean && sudo apt-get autoclean"
vagrant ssh -c "sudo dd if=/dev/zero of=/EMPTY bs=1M"
vagrant ssh -c "sudo rm -f /EMPTY"
vagrant ssh -c "cat /dev/null > ~/.bash_history && history -c && exit"

vagrant halt
vagrant package --output debian-jessie-ansible.box

echo "Use: curl 'https://atlas.hashicorp.com/api/v1/box/boonen/ansible_debian-jessie64/version/8.6.1/provider/virtualbox/upload?access_token=TOKEN' to obtain a request token."

echo "Now use 'curl -X PUT --upload-file debian-jessie-docker.box https://binstore.hashicorp.com/TOKEN' to upload the file."